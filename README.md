# Document Search Engine

This folder contains two implementations of a course project for the NPFL103 course at CUNI Prague.

  * An R implementation which explores the basics of Information Retrieval theory, applied to document corpora in Czech.
  * A Python implementation which improves the previou system by leveraging the [Xapian Search Engine](https://xapian.org/).

Check out the `results.pdf` file within each implementation for further details.