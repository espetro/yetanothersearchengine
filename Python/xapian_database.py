#!/usr/bin/env python
# February 6, 2018
# This is a script to run a Xapian-based IR system on a set of offline documents
# Follow the main .PDF file to understand the system.

import xapian_parse as xparse
import xapian_utils as xutils
import xapian
import re

wrd = re.compile(r'[a-zA-Z\-]')

def define_db(dict_docs, dbpath):
    """Creates an inverted index for the given document array"""
    db = xapian.WritableDatabase(dbpath, xapian.DB_CREATE_OR_OPEN)

    termgenerator = xapian.TermGenerator()

    for Id, Terms in dict_docs.items():
        # Create a document and assign it to the termgenerator
        doc = xapian.Document()
        termgenerator.set_document(doc)
        
        # index text for general search
        termgenerator.index_text(" ".join(Terms))
        
        doc.set_data(Id) # store original DocID
        db.add_document(doc)
    
    db.close()
    return db
# end


def search(dbpath, topics, weighting, pgsize = 500):
    """Given a database path and an array of topics, perform a search 
        for each topic on the database and retrieve the results.
        Weighting schemes are specified in the doc section
       'How to change how documents are scored' from Xapian documentation.
    """
    db = xapian.Database(dbpath)       # Load the DB
    queryparser = xapian.QueryParser() # Setup a QueryParser
    enquire = xapian.Enquire(db)       # Enquire the DB with the QP

    # Set the weighting scheme
    enquire.set_weighting_scheme(weighting)
    
    # Store each query matches (dict)
    query_matches = {}
    
    for Id, Terms in topics.items():
        # Enter an 'AND' query into the Enquire engine
        query = queryparser.parse_query(" ".join(Terms))
        enquire.set_query(query)
        
        # Get DOC metadata retrieved from each query
        query_matches[Id] = xutils.get_matches(enquire.get_mset(0, pgsize))
        
    return query_matches
    # {'query1': {'doc1': {id: X, rank: Y, score: Z}, 'doc2': [id: X, ..}, ..}
# end


def log_out(results, file, run):
    """Writes the queries results to a file. Follows TREC format."""
    # Overwrite the file with 0s
    open(file,'w').close()
    with open(file, 'a') as output:
        # Write the document ranking retrieved foreach topic
        for topID, docs in results.items():
            for _, data in docs.items():
                form = "{0} 0 {1} {2} {3} {4}\n".format(topID, data['id'],
                                                        data['rank'], data['score'], run)
                output.write(form)
#end


def run_id(key):
    """Returns the run_id identifier for file loading"""
    return wrd.sub('', key)
# end


def run_params(key):
    return {
        'run-1': [['TITLE', 'HEADING', 'TEXT'], 'stemm', slice(2,3), xapian.LMWeight()],
        'run-2': [['TITLE', 'HEADING', 'TEXT'], 'stemm', slice(2,3), xapian.BM25Weight()]
    }.get(key, [['TITLE'], 'false', 2, xapian.TfIdfWeight('nfn')])
# end


def main(args):
    """./run -q train-topics.list -d documents.list -r run-0 -o train-res-0.dat
       $0     $1 $2                $3 $4             $5 $6    $7 $8"""
    key = run_id(args[6])
    tags, mod, vertFormat, weighting = run_params(args[6])
#   print("I work from CLI!")
    
    tops, docs = xparse.read(key, args[2], args[4], vertFormat, mod,
                             TOPID = 'num', TOPTAG = ['title'],
                             DOCID = 'DOCNO', DOCTAG = tags)
#   print("Just got the documents")
    
    db = define_db(docs, 'db-{}'.format(key))
#   print("Just defined DB")
    
    results = search('db-{}'.format(key), tops, weighting, pgsize = 1000)
#   print("Just searched!")
    
    log_out(results, args[8], 'run-{}'.format(key))
    # end
    print("Everything done w/o errors. Now, you can evaluate it on the TREC eval tool!")
# end


if __name__ == "__main__":
    import sys
    main(sys.argv)
