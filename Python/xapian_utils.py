# Joaquin Terrasa Moya
# 28 feb 2018

def get_matches(qMatches):
    """Retrieve selected data from each matched doc"""
    docs = dict() # Store doc metadata
    usedDocs = set() # Check if document is already stored
    
    for match in qMatches:
        docname = match.document.get_data().decode('utf-8')
        if not docname in usedDocs: # use docname as key: it cannot be duplicated
            usedDocs.add(docname)
            docs[match.docid] = {'id': docname, 'rank': match.rank, 'score': match.weight }
        # end
    # end
    return docs
