#!/usr/bin/env python
# This is script to run a Xapian-based IR system on a set of offline documents
# Follow the main .PDF file to understand the system.

# bibliography = ['https://www.crummy.com/software/BeautifulSoup/bs4/doc/', ]

# The following functions have been modified to allow fine-tuning on:
#    query and document construction
#    lowercasing
#    stopword removal
#    weighting
#    vector normalization
#    similarity function
#    automatic query expansion
    
import re, sys, gzip, operator
import functools as ft
from czech_stemmer import cz_stem
from bs4 import BeautifulSoup
import lxml
import sys, string

# Strips down numbers and whitespaces
cleaner = re.compile(r'\d+|[ \n\t\r]+')
whites = re.compile(r'\s')
syms = re.compile(r'[^A-Za-z0-9]+')
stops = open('stopwords/stopwords-cs-nonpunct.txt')

def process(terms, mod, aggressive = False):
    """Clean the text from trivial encoding. For mod:
       'lower' applies lowercasing.
       'stops' applies stopword removal.
       'stemm' applies czech stemming."""
    
    p1 = [syms.sub(' ', x) for x in terms] # punctuation
    p2 = [cleaner.sub('', x) for x in p1]  # numbers and whitespaces
    p3 = [x for x in p2 if len(x) > 2] # drop short words
    p4 = {
        'lower': [wr.lower() for wr in p2], # lowercasing
        'stops': [wr.lower() for wr in p2 if wr.lower() not in stops], # stopword removal
        'stemm': [cz_stem(wr.lower(), aggressive) for wr in p2 if wr.lower() not in stops] # stemming
    }.get(mod, p3)

    return p4



def flatten(l):
    """Returns a flatten list."""

    if not not l and isinstance(l[0], list): # check if it has nested levels
        return list(ft.reduce(operator.add, l, []))
    return list(l)



def uncork(doc, zipped = False):
    """Returns the IO pointer to the file. 'zipped' defaults to False (for topic documents).
    Depends on: gzip"""
    
    if zipped:
        return gzip.open(doc, 'rt', encoding = 'utf-8')
    else:
        return open(doc, 'rt', encoding = 'utf-8')

    

def parse(doc_list, tagID, tagTXT,
          verF, mod, zipped):
    """
    Depends on: BeautifulSoup4, re, functools, operator, gzip"""
    
    words = {}
    for doc in doc_list:
        fl = uncork(doc, zipped)
#         print(fl)
        xml = BeautifulSoup(fl, "xml")
#         print(xml)
        tags = xml.find_all(tagTXT)
        
        if len(tags):
#             print(len(tags))
            rows = map(lambda x: x.string.split('\n'), tags) # split rows
            rows = ft.reduce(operator.add, rows, [])         # flatten nested arrays 
#             print((list(rows)))
            rows = filter(None, rows)                        # filter empty strings
            vert = map(lambda t: t.split('\t'), rows)        # split words
            term = map(lambda t: t[verF], vert)              # select vertical format words
            flat = flatten(list(term))                       # flatten nested arrays
#             print(flat)
            docTERM = process(flat, mod, aggressive = False) # process words
            
            docID = whites.sub('', xml.find(tagID).string)   # filter empty strings
#             print(xmlID)
            words[docID] = flatten(docTERM)                  # flatten nested arrays
    return words



def read(key, tops_list, docs_list, verF = 2, mod = 'false',
         TOPID = 'num', TOPTAG = ['title'], DOCID = 'DOCNO', DOCTAG = ['TITLE']):
    """Loads to memory the selected topics and documents.
       'key' is used as unique ID for the run
       'tops_list' is a list with the topic filenames
       'docs_list' is a list with the docs filenames
       'verF' follows specified vertical format
       'mod' allows for specific word cleaning. It can be '', 'lower', 'stops' or 'stemm'
       'TOPID' and 'DOCID' search for the doc ID tag
       'TOPTAG' and 'DOCTAG' search for the doc CONTENT tags"""

    # Read topic filenames
    tops_names = open(tops_list, 'rt')
    tops_names = tops_names.read().split('\n')
    tops_names = [nm for nm in tops_names if bool(nm)]
#             print(tops_names)

    # Read topics
    tops = parse(tops_names, TOPID, TOPTAG, 2, mod, zipped = False)
#             print(tops)

    # Read document filenames
    docs_names = open(docs_list, 'rt')
    docs_names = docs_names.read().split('\n')
    docs_names = [nm for nm in docs_names if bool(nm)]
    docs_names = [title + '.gz' for title in docs_names]
#             print(docs_names)

    # Read documents
    docs = parse(docs_names, DOCID, DOCTAG, verF, mod, zipped = True)
#             print(docs)

    return (tops, docs)
